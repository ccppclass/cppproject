//Including necessary libraries
#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <fstream>
#include <stdlib.h>
#include <ctype.h>
#include <cstdlib>
#include <sstream>

//Setting format to standard
using namespace std;

//Declare my functions
string selection();
string menu(string page);
string login();
void createUser();
void save();
void load();
string themeSelection();
void themeEffect();
int close();
void createCharacter();
void editCharacter();
void viewCharacter();

//Character Looks
class Looks
{
public:
    //Skin color
    string skin;
    //Hair color
    string hair;
    //Eye color
    string eyes;
    //Height
    string height;
    //Weight
    string weight;
    //Build
    string build;
    //Notable markings (tattoos, scars, etc.)
    vector<string> markings;

    //Method to set skin to user input
    void setSkin(string attribute)
    {
        skin = attribute;
    }
    //Method to set hair to user input
    void setHair(string attribute)
    {
        hair = attribute;
    }
    //Method to set eyes to user input
    void setEyes(string attribute)
    {
        eyes = attribute;
    }
    //Method to set height to user input
    void setHeight(string attribute)
    {
        height = attribute;
    }
    //Method to set weight to user input
    void setWeight(string attribute)
    {
        weight = attribute;
    }
    //Method to set build to user input
    void setBuild(string attribute)
    {
        build = attribute;
    }
    void addMarkings(string attribute)
    {
        markings.push_back(attribute);
    }
};

//Character Class (Adjusted based on theme)
class Character
{
//Declares public aspects of the class
public:
	//Character Name
	string name;
	//Character Gender
	string gender;
	//Character Age
	int age;
	//Character Race
	string race;
	//Character Personality
	vector<string> personality;
	//Character Job
	string job;
	//Character Items
	vector<string> possessions;
    //Method to set name to user input
    Looks CharacterLooks;
    void setName(string attribute)
    {
        name = attribute;
    }
    //Method to set gender to user input
    void setGender(string attribute)
    {
        gender = attribute;
    }
    //Method to set age to user input
    void setAge(int attribute)
    {
        age = attribute;
    }
    //Method to set race to user input
    void setRace(string attribute)
    {
        race = attribute;
    }
    //Method to set job to user input
    void setJob(string attribute)
    {
        job = attribute;
    }
    void addPersonality(string attribute)
    {
        personality.push_back(attribute);
    }
};

//Location Class
class Location
{
	private:
	//Location Name
	string localName;
	//Location Type
	string localType;
	//Location Landmarks
	vector<string> landmarks;
	//Location Climate
	vector<string> climate;
	//Location Culture
	vector<string> culture;
};

//Event Class
class Event
{
	private:
	//Event Identifier
	string eventName;
	//Event Type
	string eventType;
	//Event Mood
	string eventMood;
	//Characters Involved
	vector<string> charInvolved;
	//Event Location
	vector<string> eventLocal;
	//Event Time and Date
	string timeAndDate;
	//Event Causes
	vector<string> eventCauses;
	//Event Effects
	vector<string> eventEffects;
};

//User Class
class User
{
	//User name
	//Password
	//Stories Created Array
};

//Outline Class

int main()
{
	//Declare a variable for the page
	string page = "Main";
	string user;
	string newPage;

	//Introduction to program and login screen
	//user = login();

	//Loop for menu navigation
	do
	{
		//Main Menu
		newPage = menu(page);
		page = newPage;
	} while (page == "Main" || page == "Story" || page == "Characters" || page == "Locations" || page == "Events" || page == "Review");

		//New Story
			//Characters
				//New Character
				//Load Character
				//Save Character
			//Locations
				//New Location
				//Load Location
				//Save Location
			//Events
				//New Event
				//Load Event
				//Save Event
			//Review
				//View Timeline
				//View Character Relations
				//View Character Events
				//View Character Travels
		//Load Story
		//Save Story
		//Quit
}

//Selection function
string selection(string page)
{
	//Declare variables
	string userSelection;
	string newpage;

	//Collect user input
	cin >> userSelection;

	//Create space to avoid clutter
	cout << endl << endl << endl;

	//Set page based on user selection
	if (userSelection == "n" || userSelection == "N" || userSelection == "new" || userSelection == "New" || userSelection == "NEW")
	{
		//Determines which menu based on page currently on
		if(page == "Main")
		{
			//Calls themeSelection function to set the theme for the new story
			//themeSelection();

			//Sets page equal to Story to call the Story menu
			newpage = "Story";
		}
		else if(page == "Characters")
		{
			//Calls the createCharacter function to make a new character
			createCharacter();
		}
		else if(page == "Locations")
		{
			//Sets page to Location Creation to make a location
			newpage = "Location Creation";
		}
		else if(page == "Events")
		{
			//Sets page to Event Creation to make a event
			newpage = "Event Creation";
		}
		else
        {
            cout << "Invalid entry.  Please make a valid selection." << endl << endl;
            menu(page);
        }
	}
	//Determine which option the user chose to call the right menu
	else if (userSelection == "s" || userSelection == "S" || userSelection == "save" || userSelection == "Save" || userSelection == "SAVE")
	{
		//Calls save function
		save();
	}
	//Determine which option the user chose to call the right menu
	else if (userSelection == "l" || userSelection == "L" || userSelection == "load" || userSelection == "Load" || userSelection == "LOAD" || userSelection == "location" || userSelection == "Location" || userSelection == "LOCATION")
	{
		//Sets page equal to correct user input based on current page
		if (page == "Main" || userSelection == "load" || userSelection == "Load" || userSelection == "LOAD")
		{
			//Calls load function
			load();
		}
		else if (page == "Story" || userSelection == "location" || userSelection == "Location" || userSelection == "LOCATION")
		{
			//Sets page to locations
			newpage = "Locations";
		}
		else
        {
            cout << "Invalid entry.  Please make a valid selection." << endl << endl;
            menu(page);
        }
	}
	else if (userSelection == "c" || userSelection == "C" || userSelection == "characters" || userSelection == "Characters" || userSelection == "CHARACTERS")
	{
		//Sets page equal to correct user input based on current page
		if (page == "Story")
		{
			//Sets page to Characters
			newpage = "Characters";
		}
		else if (page == "Review")
		{
			newpage = "Character Relationships";
		}
		else
        {
            cout << "Invalid entry.  Please make a valid selection." << endl << endl;
            menu(page);
        }
	}
	else if (userSelection == "e" || userSelection == "E" || userSelection == "events" || userSelection == "Events" || userSelection == "EVENTS" || userSelection == "edit" || userSelection == "Edit" || userSelection == "EDIT")
	{
		//Sets page equal to correct user input based on current page
		if (page == "Story")
		{
			//Sets page to Events
			newpage = "Events";
		}
		else if (page == "Characters")
		{
			//Calls edit character function
			editCharacter();
		}
		else if (page == "Locations")
		{
			//Sets page to Location editor
			newpage = "Edit Location";
		}
		else if (page == "Events")
		{
			//Sets page to Event editor
			newpage = "Edit Event";
		}
		else if (page == "Review")
		{
			//Sets page to Character events
			newpage = "Character Events";
		}
		else
        {
            cout << "Invalid entry.  Please make a valid selection." << endl << endl;
            menu(page);
        }
	}
	else if (userSelection == "v" || userSelection == "V" || userSelection == "view" || userSelection == "View" || userSelection == "VIEW")
	{
		//Sets page equal to correct user input based on current page
		if (page == "Characters")
		{
			//calls view character function
			viewCharacter();
		}
		else if (page == "Locations")
		{
			//Sets page to Location viewer
			newpage = "View Locations";
		}
		else if (page == "Events")
		{
			//Sets page to Event viewer
			newpage = "View Events";
		}
		else if (page == "Review")
		{
			//Sets page to Character travels
			newpage = "Character Travels";
		}
		else
        {
            cout << "Invalid entry.  Please make a valid selection." << endl << endl;
            menu(page);
        }
	}
	else if (userSelection == "t" || userSelection == "T" || userSelection == "timeline" || userSelection == "Timeline" || userSelection == "TIMELINE")
	{

		if(page == "Review")
        {
            //Sets page to Timeline
            newpage = "Timeline";
        }
		else
        {
            cout << "Invalid entry.  Please make a valid selection." << endl << endl;
            menu(page);
        }
	}
	else if (userSelection == "r" || userSelection == "R" || userSelection == "review" || userSelection == "Review" || userSelection == "REVIEW")
	{
		if(page == "Story")
        {
            //Sets page to Review
            newpage = "Review";
        }
		else
        {
            cout << "Invalid entry.  Please make a valid selection." << endl << endl;
            menu(page);
        }
	}
	else if (userSelection == "b" || userSelection == "B" || userSelection == "back" || userSelection == "Back" || userSelection == "BACK")
	{
		//Determines which menu based on page currently on
		if(page == "Story")
		{
			//Sets page equal to Main to call the Main menu
			newpage = "Main";
		}

		else
		{
			//Sets page to Story
			newpage = "Story";
		}
	}
	else if (userSelection == "q" || userSelection == "Q" || userSelection == "quit" || userSelection == "Quit" || userSelection == "QUIT")
	{
		close();
	}
	else
	{
		cout << "Invalid entry.  Please make a valid selection." << endl << endl;
		menu(page);
	}

	//Returns new page
	return newpage;
}

//Menu Function
string menu(string page)
{
	//Declare variables
	string newpage;

	//What to do if on main page
	if (page == "Main")
	{
		//Displays messages to user showing which page they are on and the menu for that page
		cout << "Main Menu" << endl << endl;
		cout << "Please make a selection." << endl << endl;
		cout << "(N)ew Story --- (S)ave Story --- (L)oad Story --- (Q)uit" << endl;
	}
	//Checks if on the Story page
	else if (page == "Story")
	{
		//Displays menu for Story page
		cout << "Story Menu" << endl << endl;
		cout << "Please make a selection." << endl << endl;
		cout << "(C)haracters --- (L)ocations --- (E)vents --- (R)eview --- (B)ack" << endl;
	}
	//Checks if on the Character page
	else if (page == "Characters")
	{
		//Displays menu for Character page
		cout << "Character Menu" << endl << endl;
		cout << "Please make a selection." << endl << endl;
		cout << "(N)ew Character --- (E)dit Character --- (V)iew Characters --- (B)ack" << endl;
	}
	//Checks if on the Location page
	else if (page == "Locations")
	{
		//Displays menu for Location page
		cout << "Location Menu" << endl << endl;
		cout << "Please make a selection." << endl << endl;
		cout << "(N)ew Location --- (E)dit Location --- (V)iew Locations --- (B)ack" << endl;
	}
	//Checks if on the Event page
	else if (page == "Events")
	{
		//Displays menu for Event page
		cout << "Event Menu" << endl << endl;
		cout << "Please make a selection." << endl << endl;
		cout << "(N)ew Event --- (E)dit Event --- (V)iew Events --- (B)ack" << endl;
	}
	//Checks if on the Review page
	else if (page == "Review")
	{
		//Displays menu for Review page
		cout << "Review Menu" << endl << endl;
		cout << "Please make a selection." << endl << endl;
		cout << "View (T)imeline --- View (C)haracter Relationships --- View Character (E)vents --- (V)iew Character Travels --- (B)ack" << endl;
	}

	//Calls selection function
	newpage = selection(page);

	//Returns page selected
	menu (newpage);
	//return newpage;
}

//Login function
string login()
{
	//Declare variables for this function
	string username;
	string password;
	string userChoice;
	int count;

	//Ask the user if they want to create a profile or login
	cout << "Do you want to (L)ogin or (C)reate a profile? ";
	//Take user input
	cin >> userChoice;

	//Provide space to clean up look
	cout << endl << endl;

	//Checks user input
	if(userChoice == "L" || userChoice == "l" || userChoice == "Login" || userChoice == "login" || userChoice == "LOGIN")
	{
		//Loop point for goto loop
		tryagain:
		do
		{

			//Prompts user to enter username
			cout << "Please enter your username: ";
			cin >> username;

			//Checks username entered
			/*if()
			{
				//Prompts user to enter password
				cout << "Please enter your password: ";
				cin >> password;

				//Checks if password is correct
				if()
				{
					//Returns username
					return username;
				}
				else
				{
					//Tells user password is incorrect
					cout << "Password entered does not match our records.  Please try again.";
					//Goes back to username prompt
					goto tryagain;
					//Increases count
					count++;
				}
			}
			else
			{
				//Informs user that username provided does not exist
				cout << "Username entered is not recognized.  Please try again.";
				//Loops back to username prompt
				goto tryagain;
				//Increase count
				count++;
			}*/
		} while(count < 3);

		//Informs the user the login function has been locked
		cout << "You have provided incorrect information 3 times.  This account has been locked." << endl;
		cout << "Please try again later." << endl;
	}
	//Checks if the user needs to create a profile
	else if(userChoice == "C" || userChoice == "c" || userChoice == "Create" || userChoice == "create" || userChoice == "CREATE")
	{
		//Calls createUser function
		//createUser();
	}
	else
	{
		//Tells the user their input was invalid
		cout << "Please enter a valid option." << endl << endl;
		//Sends the user back to the beginning of the function
		login();
	}
}

//User creation function
void createUser()
{

}

//Save function
void save()
{

}

//Load function
void load()
{

}

//Theme selection function
string themeSelection()
{

}

//Function that unlock different character, event, location themes
void themeEffect()
{

}

//Function that closes the program
int close()
{
    exit (EXIT_SUCCESS);
}

void createCharacter()
{
    //Declares new object and other variables needed
    ofstream characterFile;
    ofstream characterList;
    Looks CharacterLooks;
    Character newCharacter;
    string charName;
	string charGender;
	int charAge;
	string charRace;
	string charJob;
	bool addMore = true;
	string charPersonality;
	string choiceResponse;
    string charSkin;
    string charHair;
    string charEyes;
    string charHeight;
    string charWeight;
    string charBuild;
    string charMarkings;
    string fileName;
    string page = "Characters";

    //Prompts user to give a name to the character
    cout << "Please input your new character's name: ";

    //User inputs character identifier which will become the object name
    while(getline(cin, charName))
    {

        //Checks to see if charName has a value, if it does, it ends the while loop
        if (!charName.empty()) break;
    };

    newCharacter.setName(charName);

    fileName = charName + ".txt";

    //Open character list file
    characterList.open("characterList.txt", ios::app);

    //Make sure the file connection is good
    if (characterList.good())
    {
        //Add new character name to list
        characterList << charName << endl;
    }

    //Close character list file
    characterList.close();

    characterFile.open(fileName.c_str());

    if (characterFile.good())
    {
        //Writes description to file
        characterFile << "Name: ";

        //Writes user input to file
        characterFile << charName << endl;

        //Create space
        cout << endl << endl;

        //Prompts user to give a gender to the character
        cout << "Please input your new character's gender: ";

        //User inputs character gender
        cin >> charGender;

        newCharacter.setGender(charGender);

        characterFile << "Gender: " << charGender << endl;

        //Create space
        cout << endl << endl;

        //Prompts user to give an age for the character
        cout << "Please input your new character's age: ";

        //User inputs character age
        cin >> charAge;

        newCharacter.setAge(charAge);

        characterFile << "Age: " << charAge << endl;

        //Create space
        cout << endl << endl;

        //Prompts user to give the character's race
        cout << "Please input your new character's race: ";

        //User inputs character race
        cin >> charRace;

        newCharacter.setRace(charRace);

        characterFile << "Race: " << charRace << endl;

        //Create space
        cout << endl << endl;

        //Prompts user to give the character's job
        cout << "Please input your new character's job: ";

        //Writes description to file
        characterFile << "Job: ";

        //User inputs character job
        while(getline(cin, charJob))
        {
            //Writes user input to file
            characterFile << charJob;
            //Checks to see if charJob has a value, if it does, it ends the while loop
            if (!charJob.empty()) break;
        };

        newCharacter.setJob(charJob);

        //Create space
        cout << endl << endl;

        characterFile << endl << "Personality: ";

        do
        {
           cout << "Please enter a personality trait for your character: ";

           //User inputs character personlaity
            while(getline(cin, charPersonality))
            {
                //Checks if charPersonality is empty, if it is, restarts the loop
                if (charPersonality.empty())continue;
                //Writes user input to file
                characterFile << charPersonality << ", ";
                //Checks to see if charPersonality has a value, if it does, it ends the while loop
                if (!charPersonality.empty()) break;
            };

           cout << endl << "Would you like to add any more personality traits? (Y/N) ";

           cin >> choiceResponse;

           if (choiceResponse == "N" || choiceResponse == "n" || choiceResponse == "No" || choiceResponse == "NO" || choiceResponse == "no" || choiceResponse == "nO")
           {
               addMore = false;
           }

        } while(addMore == true);

        cout << endl << endl;

        //Prompts user to give the character's skin color
        cout << "Please input your new character's skin color: ";

        //User inputs character color
        cin >> charSkin;

        newCharacter.CharacterLooks.setSkin(charSkin);

        characterFile << "Skin Color: " << charSkin << endl;

        //Create space
        cout << endl << endl;

        //Prompts user to give the character's hair color
        cout << "Please input your new character's hair color: ";

        //User inputs character hair color
        cin >> charHair;

        newCharacter.CharacterLooks.setHair(charHair);

        characterFile << "Hair Color: " << charHair << endl;

        //Create space
        cout << endl << endl;

        //Prompts user to give the character's eye color
        cout << "Please input your new character's eye color: ";

        //User inputs character eye color
        cin >> charEyes;

        newCharacter.CharacterLooks.setEyes(charEyes);

        characterFile << "Eye Color: " << charEyes << endl;

        //Create space
        cout << endl << endl;

        //Prompts user to give the character's height
        cout << "Please input your new character's height: ";

        //Writes description to file
        characterFile << "Height: ";

        //User inputs character height
        while(getline(cin, charHeight))
        {
            //Writes user input to file
            characterFile << charHeight;
            //Checks to see if charHeight has a value, if it does, it ends the while loop
            if (!charHeight.empty()) break;
        };

        newCharacter.CharacterLooks.setHeight(charHeight);

        //Create space
        cout << endl << endl;

        //Prompts user to give the character's weight
        cout << "Please input your new character's weight: ";


        //Writes description to file
        characterFile << endl << "Weight: ";

        //User inputs character weight
        while(getline(cin, charWeight))
        {
            //Writes user input to file
            characterFile << charWeight;
            //Checks to see if charWeight has a value, if it does, it ends the while loop
            if (!charWeight.empty()) break;
        };

        //Sets character weight to the object created
        newCharacter.CharacterLooks.setWeight(charWeight);

        //Create space
        cout << endl << endl;

        //Prompts user to give the character's build
        cout << "Please input your new character's build: ";

        //User inputs character build
        cin >> charBuild;

        newCharacter.CharacterLooks.setBuild(charBuild);

        characterFile << endl << "Build: " << charBuild << endl;

        //Create space
        cout << endl << endl;

        characterFile << "Markings: ";

        do
        {
           cout << "Please enter a defining mark (tattoo, scar, etc.) for your character: ";

           //User inputs character markings
            while(getline(cin, charMarkings))
            {
                //Checks if charMarkings is empty, if it is, restarts the loop
                if (charMarkings.empty())continue;
                //Writes user input to file
                characterFile << charMarkings << ", ";
                //Checks to see if charMarkings has a value, if it does, it ends the while loop
                if (!charMarkings.empty()) break;
            };

           cout << endl << "Would you like to add any more markings? (Y/N) ";

           cin >> choiceResponse;

           try
           {
               if (choiceResponse != "Y" || choiceResponse != "y" || choiceResponse != "n" || choiceResponse != "N")
               {
                   throw 1;
               }
           }
           catch(int number)
           {
               cout << endl << "ERROR: Please input a valid one character option." << endl;
           }

           cout << endl;

           if (choiceResponse == "N" || choiceResponse == "n")
           {
               addMore = false;
           }
           else if (choiceResponse == "Y" || choiceResponse == "y")
           {
               addMore = true;
           }


        } while(addMore == true);

        cout << endl << endl;

        characterFile.close();

        menu(page);
    }
    else
    {
        cout << "File could not be opened." << endl;

        createCharacter();
    }
}

void editCharacter()
{
    string text;
    string page = "Characters";
    string userInput;
    int selection;
    int charIndex = 0;
    int option = 1;
    vector<string> charName;
    string returnChoice;
    bool done = false;
    int arrayIndex;
    string fileName;
    string newInput;
    string newLine;
    int counter = 1;

    //Create an object for the class of ifstream to read files
    fstream CharacterList;
    fstream CharacterInfo;

    //Open character list file
    CharacterList.open("characterList.txt");

    //Checks if the file exists
    if (CharacterList.good())
    {
        while (getline (CharacterList, text))
        {
            cout << charIndex + 1 << ") " << text << '\n';
            charName.push_back(text);
            charIndex++;
        }

        CharacterList.close();
    }
    else
    {
        cout << "File not found.";
    }

     //prompts user to choose a character to edit
    cout << endl << "Choose the number of the character you want to edit: " << endl;

    //User inputs their choice
    cin >> selection;
    cout << endl;

    //Convert user input into the array index
    arrayIndex = selection - 1;

    //Convert the filename from the vector indexed
    fileName = charName[arrayIndex] + ".txt";

    //Opens chosen character information file
    CharacterInfo.open(fileName.c_str());

    //Check if the file exists
    if (CharacterInfo.good())
    {
        while (getline (CharacterInfo, text))
        {
            cout << option << ") " << text << '\n';
            option ++;
        }

        CharacterInfo.close();
    }
    else
    {
        cout << "File not found.";
    }

    cout << endl << "Please enter the number that represents the attribute you want to edit, or enter 'D' to delete this character: ";

    cin >> userInput;

    //Provide results for user selection
    if (userInput == "d" || userInput == "D")
    {
        ofstream temp;
        temp.open("temp.txt");


        CharacterList.open("characterList.txt");
        if(CharacterList.good())
        {
            while(getline(CharacterList,text))
            {
                if(text != charName[arrayIndex])
                {
                   temp << text << endl;
                }
            }

            temp.close();
            CharacterList.close();
            remove("characterList.txt");
            rename("temp.txt", "characterList.txt");
        }
        else
        {
            cout << "File not found.";
        }
        remove(fileName.c_str());
    }
    else
    {
        //selection = atoi(userInput.c_str());

        //istringstream convert(userInput);

        if (!(istringstream(userInput) >> selection))
        {
            selection = 0;
        }

        CharacterInfo.open(fileName.c_str());
        ofstream temp;
        temp.open("temp.txt");

        cout << endl << selection;



        if(CharacterInfo.good())
        {
            while (selection > 13 || selection < 1)
            {
                cout << "Please input a valid option: ";
                cin >> selection;
                cout << endl;
            }
            switch (selection)
            {
                case '1':
                    cout << "What would you like the new name to be: ";
                    while(getline(cin, newInput))
                    {
                           if (!newInput.empty()) break;
                    };
                    newLine = "Name: " + newInput;
                    break;
                case '2':
                    cout << "What would you like the new gender to be: ";
                    while(getline(cin, newInput))
                    {
                           if (!newInput.empty()) break;
                    };
                    newLine = "Gender: " + newInput;
                    break;
                case '3':
                    cout << "What would you like the new age to be: ";
                    while(getline(cin, newInput))
                    {
                           if (!newInput.empty()) break;
                    };
                    newLine = "Age: " + newInput;
                    break;
                case '4':
                    cout << "What would you like the new race to be: ";
                    while(getline(cin, newInput))
                    {
                           if (!newInput.empty()) break;
                    };
                    newLine = "Race: " + newInput;
                    break;
                case '5':
                    cout << "What would you like the new job to be: ";
                    while(getline(cin, newInput))
                    {
                           if (!newInput.empty()) break;
                    };
                    newLine = "Job: " + newInput;
                    break;
                case '6':
                    cout << "What would you like the new personality traits to be: ";
                    while(getline(cin, newInput))
                    {
                           if (!newInput.empty()) break;
                    };
                    newLine = "Personality: " + newInput;
                    break;
                case '7':
                    cout << "What would you like the new skin color to be: ";
                    while(getline(cin, newInput))
                    {
                           if (!newInput.empty()) break;
                    };
                    newLine = "Skin Color: " + newInput;
                    break;
                case '8':
                    cout << "What would you like the new hair color to be: ";
                    while(getline(cin, newInput))
                    {
                           if (!newInput.empty()) break;
                    };
                    newLine = "Hair Color: " + newInput;
                    break;
                case '9':
                    cout << "What would you like the new eye color to be: ";
                    while(getline(cin, newInput))
                    {
                           if (!newInput.empty()) break;
                    };
                    newLine = "Eye Color: " + newInput;
                    break;
                case '10':
                    cout << "What would you like the new height to be: ";
                    while(getline(cin, newInput))
                    {
                           if (!newInput.empty()) break;
                    };
                    newLine = "Height: " + newInput;
                    break;
                case '11':
                    cout << "What would you like the new weight to be: ";
                    while(getline(cin, newInput))
                    {
                           if (!newInput.empty()) break;
                    };
                    newLine = "Weight: " + newInput;
                    break;
                case '12':
                    cout << "What would you like the new build to be: ";
                    while(getline(cin, newInput))
                    {
                           if (!newInput.empty()) break;
                    };
                    newLine = "Build: " + newInput;
                    break;
                case '13':
                    cout << "What would you like the new markings to be: ";
                    while(getline(cin, newInput))
                    {
                           if (!newInput.empty()) break;
                    };
                    newLine = "Markings: " + newInput;
                    break;
            }
            while(getline(CharacterInfo,text))
            {
                if(counter != selection)
                {
                   temp << text << endl;
                }
                else
                {
                    temp << newLine << endl;
                }
                counter++;
            }

            temp.close();
            CharacterInfo.close();
            remove(fileName.c_str());
            rename("temp.txt", fileName.c_str());
        }
        else
        {
            cout << "File not found!";
        }

    }

    do
    {
        cout << endl << endl << "Type B for back to choose another character to view, or C to return to the Character page: ";

        cin >> returnChoice;

        if (returnChoice == "B" || returnChoice == "b")
        {
            editCharacter();
            done = true;
        }
        else if (returnChoice == "C" || returnChoice == "c")
        {

            done = true;
        }
        else
        {
            cout << "Please enter a valid option.";
        }
    } while (done == false);

    menu(page);
}

void viewCharacter()
{
    string text;
    string page = "Characters";
    int userInput;
    int charIndex = 0;
    vector<string> charName;
    string returnChoice;
    bool done = false;
    int arrayIndex;
    string fileName;

    //Create an object for the class of ifstream to read files
    ifstream CharacterList;
    ifstream CharacterInfo;

    //Open character list file
    CharacterList.open("characterList.txt");

    //Checks if the file exists
    if (CharacterList.good())
    {
        while (getline (CharacterList, text))
        {
            cout << charIndex + 1 << ") " << text << '\n';
            charName.push_back(text);
            charIndex++;
        }

        CharacterList.close();
    }
    else
    {
        cout << "File not found.";
    }



    //prompts user to choose a character to view
    cout << endl << "Choose the number of the character you want to view: ";

    //User inputs their choice
    cin >> userInput;

    //Convert user input into the array index
    arrayIndex = userInput - 1;

    //Convert the filename from the vector indexed
    fileName = charName[arrayIndex] + ".txt";

    //Opens chosen character information file
    CharacterInfo.open(fileName.c_str());

    //Check if the file exists
    if (CharacterInfo.good())
    {
        while (getline (CharacterInfo, text))
        {
            cout << text << '\n';
        }

        CharacterInfo.close();
    }
    else
    {
        cout << "File not found.";
    }

    do
    {
        cout << endl << endl << "Type B for back to choose another character to view, or C to return to the Character page: ";

        cin >> returnChoice;

        if (returnChoice == "B" || returnChoice == "b")
        {
            viewCharacter();
            done = true;
        }
        else if (returnChoice == "C" || returnChoice == "c")
        {

            done = true;
        }
        else
        {
            cout << "Please enter a valid option.";
        }
    } while (done == false);

    menu(page);
}
